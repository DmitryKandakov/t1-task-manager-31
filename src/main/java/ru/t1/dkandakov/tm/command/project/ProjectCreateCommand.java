package ru.t1.dkandakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create a new project.";
    }

}