package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.model.Task;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task list by project id";
    }

}