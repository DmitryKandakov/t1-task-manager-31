package ru.t1.dkandakov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFildException {

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}
