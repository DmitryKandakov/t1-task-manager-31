package ru.t1.dkandakov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkandakov.tm.dto.request.ServerAboutRequest;
import ru.t1.dkandakov.tm.dto.request.ServerVersionRequest;
import ru.t1.dkandakov.tm.dto.response.ServerAboutResponse;
import ru.t1.dkandakov.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

}