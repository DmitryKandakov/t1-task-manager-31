package ru.t1.dkandakov.tm.client;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.Socket;

@Getter
public abstract class AbstractEndpoint {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull
    private Socket socket;

    public AbstractEndpoint() {
    }

    public AbstractEndpoint(@NotNull String host, @NotNull Integer port) {
        this.host = host;
        this.port = port;
    }

    @NotNull
    protected Object call(@NotNull final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }

}