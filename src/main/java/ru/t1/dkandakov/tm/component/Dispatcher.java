package ru.t1.dkandakov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.endpoint.IOperation;
import ru.t1.dkandakov.tm.dto.request.AbstractRequest;
import ru.t1.dkandakov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, IOperation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final IOperation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        final IOperation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}