package ru.t1.dkandakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.dkandakov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkandakov.tm.api.repository.ICommandRepository;
import ru.t1.dkandakov.tm.api.repository.IProjectRepository;
import ru.t1.dkandakov.tm.api.repository.ITaskRepository;
import ru.t1.dkandakov.tm.api.repository.IUserRepository;
import ru.t1.dkandakov.tm.api.service.*;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.dto.request.ServerAboutRequest;
import ru.t1.dkandakov.tm.dto.request.ServerVersionRequest;
import ru.t1.dkandakov.tm.endpoint.SystemEndpoint;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkandakov.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.model.User;
import ru.t1.dkandakov.tm.repository.CommandRepository;
import ru.t1.dkandakov.tm.repository.ProjectRepository;
import ru.t1.dkandakov.tm.repository.TaskRepository;
import ru.t1.dkandakov.tm.repository.UserRepository;
import ru.t1.dkandakov.tm.service.*;
import ru.t1.dkandakov.tm.util.SystemUtil;
import ru.t1.dkandakov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.dkandakov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private  final  Server server = new Server(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    public void run(@NotNull String[] args) {
        prepareStartup();
        processArguments(args);
        processCommands();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("BETA PROJECT", Status.COMPLETED));

        taskService.create(test.getId(), "MEGA TASK", "111111");
        taskService.create(test.getId(), "BETA TASK", "222222");
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom((clazz))) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

        @SneakyThrows
        private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            @NotNull final String argument = args[0];
            processArgument(argument);
            loggerService.command(argument);
            System.exit(0);
        } catch (@NotNull final Exception e) {
            System.err.println("[FAIL]");
            loggerService.error(e);
            System.exit(1);
        }
    }

    public void processArgument(@NotNull final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@NotNull final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void processCommand(@NotNull final String command) {
        processCommand(command, true);
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
        fileScanner.stop();
        server.stop();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
        server.start();
    }

}